package com.Lab_8.Atividade_8.controllers;

import com.Lab_8.Atividade_8.dto.ProductDetailsResponse;
import com.Lab_8.Atividade_8.dto.ProductRequest;
import com.Lab_8.Atividade_8.dto.ProductResponse;
import com.Lab_8.Atividade_8.services.ProductService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping
    public ResponseEntity<ProductResponse> createProduct(@RequestBody @Valid ProductRequest request){
        ProductResponse response = productService.create(request);
        response.add(linkTo(methodOn(ProductController.class).getProduct(response.getId())).withSelfRel());
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @GetMapping("/{id}")
    @Cacheable(value = "product", key = "#id") // Adiciona cache para esta operação
    public ResponseEntity<ProductDetailsResponse> getProduct(@PathVariable(value = "id") String id){
        ProductDetailsResponse result = productService.getById(id);
        if(result == null)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PutMapping("/{id}")
    @CachePut(value = "product", key = "#id") // Atualiza o cache após a operação
    public ResponseEntity<ProductResponse> updateProduct(@PathVariable(value = "id") String id, @RequestBody @Valid ProductRequest request){
        ProductResponse result = productService.update(request, id);
        if(result == null)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @DeleteMapping("/{id}")
    @CacheEvict(value = "product", key = "#id") // Remove do cache após a operação
    public ResponseEntity<Void> deleteProduct(@PathVariable(value = "id") String id){
        boolean deleted = productService.delete(id);
        if(deleted)
            return ResponseEntity.status(HttpStatus.OK).build();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
