package com.Lab_8.Atividade_8.entities;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

//Old Relational usages:
//@Entity
//@Table(name="TB")
//@Column(name = "")
//@Column(name = "")

@Document(collection = "tb_users")
public class Products extends NoRelational {
    @Field("name")
    private String name;
    @Field("price")
    private double price;

    @Field("data sheet")
    private String data_sheet;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getData_sheet() {
        return data_sheet;
    }

    public void setData_sheet(String data_sheet) {
        this.data_sheet = data_sheet;
    }
}