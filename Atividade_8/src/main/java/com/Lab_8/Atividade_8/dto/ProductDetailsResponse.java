package com.Lab_8.Atividade_8.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public record ProductDetailsResponse(
        String id,
        String name,
        double price,
        String data_sheet,
        LocalDateTime createdAt,
        LocalDateTime updatedAt

) implements Serializable {

}

