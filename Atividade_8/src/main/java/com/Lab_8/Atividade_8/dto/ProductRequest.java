package com.Lab_8.Atividade_8.dto;



import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

public record ProductRequest(
        @NotNull String name,
        @Positive double price,
        @NotNull String data_sheet
) {}