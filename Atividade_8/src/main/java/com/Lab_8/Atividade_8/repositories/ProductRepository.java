package com.Lab_8.Atividade_8.repositories;


import com.Lab_8.Atividade_8.entities.Products;
import org.springframework.data.mongodb.repository.MongoRepository;

//Old Relatioal usage:
//@Repository
//public interface ProductRepository extends JpaRepository<Product, UUID> {}





public interface ProductRepository extends MongoRepository<Products, String> {}
