package com.Lab_8.Atividade_8.services;

import com.Lab_8.Atividade_8.dto.ProductDetailsResponse;
import com.Lab_8.Atividade_8.dto.ProductRequest;
import com.Lab_8.Atividade_8.dto.ProductResponse;
import com.Lab_8.Atividade_8.entities.Products;
import com.Lab_8.Atividade_8.factorie.ProductFactory;
import com.Lab_8.Atividade_8.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;





@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public ProductResponse create(ProductRequest request){
        Products product = new Products();
        product.setName(request.name());
        product.setPrice(request.price());
        product.setData_sheet(request.data_sheet());

        Products result = productRepository.save(product);

        return ProductFactory.createResponse(result);
    }

    public ProductDetailsResponse getById(String id){
        Optional<Products> result = productRepository.findById(id);
        return result.map(ProductFactory::createDetailsResponse).orElse(null);
    }

    public ProductResponse update(ProductRequest dto, String id){
        Optional<Products> result = productRepository.findById(id);
        if(result.isEmpty()) return null;

        Products toUpdate = result.get();
        toUpdate.setName(dto.name());
        toUpdate.setPrice(dto.price());
        toUpdate.setData_sheet(dto.data_sheet());
        toUpdate.update();

        Products saved = productRepository.save(toUpdate);

        return ProductFactory.createResponse(saved);
    }

    public boolean delete(String id){
        Optional<Products> result = productRepository.findById(id);
        if(result.isEmpty()) return false;
        productRepository.deleteById(id);
        return true;
    }
}
