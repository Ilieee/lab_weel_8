package com.Lab_8.Atividade_8.factorie;

import com.Lab_8.Atividade_8.dto.ProductDetailsResponse;
import com.Lab_8.Atividade_8.dto.ProductResponse;
import com.Lab_8.Atividade_8.entities.Products;


public class ProductFactory {

    public static ProductDetailsResponse createDetailsResponse(Products product) {
        return new ProductDetailsResponse(
                product.getStringId(),
                product.getName(),
                product.getPrice(),
                product.getData_sheet(),
                product.getCreatedAt(),
                product.getUpdatedAt()
        );
    }

    public static ProductResponse createResponse(Products product) {
        ProductResponse dto = new ProductResponse();
        dto.setId(product.getStringId());
        dto.setName(product.getName());
        dto.setPrice(product.getPrice());
        dto.setData_sheet(product.getData_sheet());
        dto.setCreatedAt(product.getCreatedAt());
        dto.setUpdatedAt(product.getUpdatedAt());
        return dto;
    }
}


