# Projeto Atividade_8

## Descrição
Este projeto é uma aplicação para gerenciar produtos. Ele fornece endpoints para criar, recuperar, atualizar e excluir produtos.

## Tecnologias Utilizadas
- Java
- Spring Boot
- MongoDB (ou outro banco de dados NoSQL)
- Maven

## Estrutura do Projeto
O projeto segue uma estrutura padrão de aplicativo Spring Boot,com classes para controladores, serviços, DTOs (Objetos de Transferência de Dados) e entidades. Ele também inclui configurações para cache e dependências gerenciadas pelo Maven.

## Endpoints
- **Criar um novo produto**:
  - Método: `POST`
  - URL: `/products`
  - Corpo da solicitação: Um objeto JSON contendo os detalhes do produto, como nome, preço e folha de dados.

- **Obter detalhes de um produto existente**:
  - Método: `GET`
  - URL: `/products/{id}`
  - Parâmetro de caminho: `id` é o ID do produto que você deseja obter.

- **Atualizar um produto existente**:
  - Método: `PUT`
  - URL: `/products/{id}`
  - Parâmetro de caminho: `id` é o ID do produto que você deseja atualizar.
  - Corpo da solicitação: Um objeto JSON contendo os detalhes atualizados do produto, como nome, preço e folha de dados.

- **Excluir um produto existente**:
  - Método: `DELETE`
  - URL: `/products/{id}`
  - Parâmetro de caminho: `id` é o ID do produto que você deseja excluir.

